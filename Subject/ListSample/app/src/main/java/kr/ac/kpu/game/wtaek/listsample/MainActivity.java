package kr.ac.kpu.game.wtaek.listsample;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import kr.ac.kpu.game.wtaek.listsample.data.HighscoreItem;
import kr.ac.kpu.game.wtaek.listsample.data.Serializer;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ListView listView;

    private String[] resId = {"1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg"};
    private ArrayList<HighscoreItem> scores = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        scores.add(new HighscoreItem("Hello",new Date(), 22340));
//        scores.add(new HighscoreItem("World",new Date(), 1340));
//        scores.add(new HighscoreItem("Babo",new Date(), 340));
        scores = Serializer.load(this);

        listView = findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }

    BaseAdapter adapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return scores.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Log.d(TAG, "Pos" + position + " convertView:" + convertView);
            View view = convertView;
            if (view == null) {
                Log.d(TAG, "Loading from XML for: " + position);
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.score_item, null);
            }
            // onCreate
            TextView tv = view.findViewById(R.id.scoreItemTextView);
            TextView tvDate = view.findViewById(R.id.scoreItemTextViewDate);
            ImageView iv = view.findViewById(R.id.imageView);

            HighscoreItem s = scores.get(position);
            tv.setText(s.name + " / " + s.score);

            String dstr = getLocalTimeString(s.date);
            tvDate.setText(dstr);

            AssetManager assets = getAssets();
            try {
                Random rand = new Random();
                int i = rand.nextInt(5);
                InputStream is = assets.open("userImage/" + resId[i]);
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                iv.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return view;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


    };

    private String getLocalTimeString(Date date) {
        String fmt = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat df = new SimpleDateFormat(fmt);
        df.setTimeZone(TimeZone.getTimeZone("KST"));
        return df.format(date);
    }

    public void onBtnAdd(View view) {
        String strUri = "tel:02120";
        Uri uri = Uri.parse(strUri);
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
//            requestPermissions();
            return;
        }
        startActivity(intent);

//        Toast toast = Toast.makeText(this, R.string.add_highscore_message, Toast.LENGTH_LONG);
//        toast.show();

//        final EditText et = new EditText(this);
//        new AlertDialog.Builder(this)
//                .setTitle(R.string.highscore)
//                .setMessage(R.string.add_highscore_message)
//                .setView(et)
//                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String name = et.getText().toString();
//                        int score = new Random().nextInt(100000);
//                        addHighscore(name, score);
//                    }
//                })
//                .setNegativeButton(R.string.cancel, null)
//                .create()
//                .show();
    }

    private void addHighscore(String name, int score) {
        scores.add(new HighscoreItem(name,new Date(), score));
        Serializer.save(this,scores);
//        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void onBtnDelete(View view) {

    }
}
