package kr.ac.kpu.game.wtaek.blocksample.game.iface;

public interface Recyclable {
    public void recycle();
}
