package kr.ac.kpu.game.wtaek.blocksample.game.iface;

import android.graphics.Canvas;

public interface GameObject {
    public void update();
    public void draw(Canvas canvas);
}
