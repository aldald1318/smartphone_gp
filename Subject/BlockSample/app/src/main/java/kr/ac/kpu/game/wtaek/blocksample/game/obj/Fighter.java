package kr.ac.kpu.game.wtaek.blocksample.game.obj;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.Log;
import android.view.animation.AnticipateInterpolator;

import kr.ac.kpu.game.wtaek.blocksample.R;
import kr.ac.kpu.game.wtaek.blocksample.game.framework.GameWorld;
import kr.ac.kpu.game.wtaek.blocksample.game.iface.GameObject;
import kr.ac.kpu.game.wtaek.blocksample.game.world.MainWorld;
import kr.ac.kpu.game.wtaek.blocksample.res.sound.SoundEffects;
import kr.ac.kpu.game.wtaek.blocksample.res.bitmap.FrameAnimationBitmap;

public class Fighter implements GameObject {
    private static final String TAG = Fighter.class.getSimpleName();
    public static final int FRAMES_PER_SECOND = 10;
    public static final int FRAME_COUNT = 5;
    private final FrameAnimationBitmap fabIdle;
    private final FrameAnimationBitmap fabShoot;
    private final int halfSize;
    private final int shootOffset;
    private float x;
    private float y;

    public void setScale(float scale){
        Log.v(TAG, "setScale: "+scale);
        this.scale = scale;
    }

    private float scale;
//    private long firedOn;

    public void fire() {
        if (state != State.idle) {
            return;
        }

//        firedOn = GameWorld.get().getCurrentTimeNanos();
        ObjectAnimator oa = ObjectAnimator.ofFloat(this,"scale",1.0f,2.0f);
        oa.setDuration(300);
        oa.setInterpolator(new AnticipateInterpolator());
        oa.start();
        state = State.shoot;
        fabShoot.reset();
        SoundEffects.get().play(R.raw.hadouken);
    }

    private void addFireBall(){
        int height = fabIdle.getHeight();
        int fx = (int)( x + height * 0.80f );
        int fy = (int)( y - height * 0.05f );

        int speed = halfSize / 10;
        MainWorld gw = MainWorld.get();
        FireBall fb = new FireBall(fx, fy,speed);
        gw.add(MainWorld.Layer.missile, fb);
    }

    private enum State{
        idle, shoot
    }
    private State state = State.idle;
    public Fighter(float x, float y) {
        MainWorld gw = MainWorld.get();
        Resources res = gw.getResources();
        fabIdle = new FrameAnimationBitmap(R.mipmap.ryu, FRAMES_PER_SECOND, FRAME_COUNT);
        fabShoot = new FrameAnimationBitmap(R.mipmap.ryu_1, FRAMES_PER_SECOND, FRAME_COUNT);
        shootOffset = fabShoot.getHeight()* 32 /100;
        halfSize = fabIdle.getHeight() / 2;
        Log.d(TAG,"halfSize = " + halfSize);
        this.x = x;
        this.y = y;

        Context context = gw.getContext();
    }

    @Override
    public void update() {
        if(state == State.shoot) {
            boolean done = fabShoot.done();
            if (done) {
                state = State.idle;
                fabIdle.reset();
                addFireBall();
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if(state == State.idle){
            fabIdle.draw(canvas,x,y);
        } else{
            float now = GameWorld.get().getCurrentTimeNanos();
//            float scale = (float) (1 + (now - firedOn) / 1_000_000_000.0);
            canvas.save();
            canvas.scale(scale, scale,x,y);
            fabShoot.draw(canvas,x + shootOffset,y);
            canvas.restore();
        }
    }


}
