package kr.ac.kpu.game.wtaek.blocksample.game.iface;

import android.graphics.RectF;

public interface BoxCollidable {
    public void getBox(RectF rect);
}
