package kr.ac.kpu.game.wtaek.pairgame;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int[] BUTTON_IDS = {
            R.id.b_00, R.id.b_01, R.id.b_02, R.id.b_03,
            R.id.b_10, R.id.b_11, R.id.b_12, R.id.b_13,
            R.id.b_20, R.id.b_21, R.id.b_22, R.id.b_23,
            R.id.b_30, R.id.b_31, R.id.b_32, R.id.b_33,
            R.id.b_40, R.id.b_41, R.id.b_42, R.id.b_43,
            R.id.b_50, R.id.b_51, R.id.b_52, R.id.b_53,
    };
    private static final int[] IMAGE_RES_IDS = {
      R.mipmap.card_2c, R.mipmap.card_3d, R.mipmap.card_4h, R.mipmap.card_5s,
      R.mipmap.card_jc, R.mipmap.card_qh, R.mipmap.card_kd, R.mipmap.card_as,
      R.mipmap.card_ah, R.mipmap.card_5d, R.mipmap.card_4c, R.mipmap.card_3c,
    };
    private ImageButton lastButton;
    private int flips;
    private TextView scoreTextView;
    private String scoreFormatString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectViews();

        startGame();
    }

    private void connectViews() {
        scoreTextView = findViewById(R.id.scoreTextView);
        Resources res = getResources();
        scoreFormatString = res.getString(R.string.flips_fmt);
    }

    private void startGame() {
        int[] buttonIDs = shuffleButtonIds();

        for(int i = 0; i < BUTTON_IDS.length; ++i){
            ImageButton btn = findViewById(buttonIDs[i]);
            int resID = IMAGE_RES_IDS[i/2];
            btn.setTag(resID);
            btn.setImageResource(R.mipmap.card_blue_back);
            btn.setScaleType(ImageView.ScaleType.FIT_CENTER);
            btn.setEnabled(true);
        }

        lastButton = null;
        flips = 0;
        showScore();
    }

    private void showScore() {

        String text = String.format(scoreFormatString, flips);
        scoreTextView.setText(text);
    }

    private int[] shuffleButtonIds() {
        int[] buttonIds = Arrays.copyOf(BUTTON_IDS, BUTTON_IDS.length);
        Random rand = new Random();
        for(int i=0; i<BUTTON_IDS.length; ++i) {
            int r = rand.nextInt(buttonIds.length);
            int temp = buttonIds[i];
            buttonIds[i] = buttonIds[r];
            buttonIds[r] = temp;
        }
        return buttonIds;
    }

    public void onBtnItem(View view) {
        Log.d(TAG, "Button ID = " + view.getId());
        Log.d(TAG, "Button Index = " + (view.getId()-R.id.b_00));

        ImageButton btn = (ImageButton) view;
        int resID = (int) btn.getTag();
        btn.setImageResource(resID);
        btn.setEnabled(false);

        if(lastButton == null){
            lastButton = btn;
            return;
        }

        if((int)lastButton.getTag() == (int)btn.getTag()){
            lastButton = null;
            return;
        }

        lastButton.setImageResource(R.mipmap.card_blue_back);
        lastButton.setEnabled(true);
        lastButton = btn;

        flips++;
        showScore();
    }

    public void onBtnRestart(View view) {
        Log.d(TAG, "onBtnRestart()");
//        startGame();
        new AlertDialog.Builder(this)
                .setTitle(R.string.restartTitle)
                .setMessage(R.string.restartMessage)
                .setNegativeButton(R.string.restartNo, null)
                .setPositiveButton(R.string.restartYes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startGame();
                    }
                })
                .create()
                .show();
    }

}
