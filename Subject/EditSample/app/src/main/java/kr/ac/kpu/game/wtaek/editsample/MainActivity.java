package kr.ac.kpu.game.wtaek.editsample;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private TextView textView;
    private Animation rotateAnim;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PieView pieView = new PieView(this);

        FrameLayout parentView = findViewById(R.id.pieParent);
        parentView.addView(pieView);

        editText = findViewById(R.id.editText);
        textView = findViewById(R.id.textView);

        editText.setOnEditorActionListener(editorActionListener);

        final TextView animTextView = findViewById(R.id.animTextView);
        animTextView.post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable funnydog = (AnimationDrawable) animTextView.getBackground();
                funnydog.start();
            }
        });

        rotateAnim = AnimationUtils.loadAnimation(this,R.anim.rotate);

        handler = new Handler();

        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.v("MainActivity", "Later");
            }
        });

        Log.v("MainActivity", "Who first?");
    }

    private TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            String text = editText.getText().toString();
            textView.setText("With Editor Action: " + text);
            return false;
        }
    };

    public void onBtnPush(View view) {
        String text = editText.getText().toString();
        textView.setText("You entered: " + text);
        textView.startAnimation(rotateAnim);
    }
}
