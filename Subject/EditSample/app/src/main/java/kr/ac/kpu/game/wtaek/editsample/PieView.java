package kr.ac.kpu.game.wtaek.editsample;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

public class PieView extends View {
    private static final String TAG = PieView.class.getSimpleName();
    private float rectX;

    public PieView(Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() != MotionEvent.ACTION_DOWN)
            return false;

        final ValueAnimator anim = ValueAnimator.ofFloat(100,500);
        anim.setDuration(2000);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                rectX = (Float)animation.getAnimatedValue();
                invalidate();;
            }
        });
        anim.start();;

        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int w =getWidth();
        int h = getHeight();
        int pl = getPaddingLeft();
        int pt = getPaddingTop();
        Log.d(TAG, "onDraw()");

        Resources res = getResources();
        Bitmap b = BitmapFactory.decodeResource(res, R.mipmap.dog_photo1);
        Matrix m = new Matrix();
        m.preScale(2.0f,1.5f);
        m.preTranslate(100f,200f);
        m.preRotate(45f);
        canvas.drawBitmap(b,m,null);

        Paint paint =new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5.0f);
        canvas.drawLine(pl,pt,w,h,paint);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            paint.setAntiAlias(false);
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(0x7F003FFF);
            canvas.drawRoundRect(rectX,30,w/2,h/2,30,30,paint);
        }

        //Text
        paint.setTypeface(Typeface.create(Typeface.MONOSPACE,Typeface.NORMAL));
        paint.setTextSize(40);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText("Hello",3*w/4,h/4,paint);

        super.onDraw(canvas);
    }

}
