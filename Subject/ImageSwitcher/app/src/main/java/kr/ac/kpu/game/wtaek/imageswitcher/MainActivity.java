package kr.ac.kpu.game.wtaek.imageswitcher;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private int pageNumber;
    private static int[] IMAGE_RES_IDS = {
            R.mipmap.dog_photo1, R.mipmap.dog_photo2, R.mipmap.dog_photo3,
            R.mipmap.dog_photo4, R.mipmap.dog_photo5, R.mipmap.dog_photo6, R.mipmap.dog_photo7,
    };
    private ImageView mainImageView;
    private TextView headerTextView;
    private String headerFormatString;
    private ImageButton prevButton;
    private ImageButton nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainImageView = findViewById(R.id.mainImageView);
        headerTextView = findViewById(R.id.headerTextView);
        prevButton  = findViewById(R.id.prevButton);
        nextButton = findViewById(R.id.nextButton);

        Resources res = getResources();
        headerFormatString = res.getString(R.string.header_title_fmt);

        pageNumber = 1;
        showPage();
    }

    public void onBtnPrev(View view) {
//        if(pageNumber <= 1) return;
        pageNumber--;
        showPage();
    }

    public void onBtnNext(View view) {
//        if(pageNumber >= IMAGE_RES_IDS.length) return;
        pageNumber++;
        showPage();
    }

    private void showPage() {
        prevButton.setEnabled(pageNumber>1);
        nextButton.setEnabled(pageNumber<IMAGE_RES_IDS.length);


        int resID = IMAGE_RES_IDS[pageNumber-1];
        mainImageView.setImageResource(resID);
        String text = String.format(headerFormatString, pageNumber,IMAGE_RES_IDS.length);
        headerTextView.setText(text);
    }
}
