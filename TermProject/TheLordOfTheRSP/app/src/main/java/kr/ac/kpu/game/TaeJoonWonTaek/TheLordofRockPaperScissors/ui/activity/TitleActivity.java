package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.Constant;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.database.DBHandler;

public class TitleActivity extends AppCompatActivity {

    private static final String TAG = TitleActivity.class.getSimpleName();
    private EditText editTextID;
    private EditText editTextPW;
    private static String strID;
    private static String strPW;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);
        Constant.currentContext = this;

        editTextID = (EditText) findViewById(R.id.inputID);
        editTextPW = (EditText) findViewById(R.id.inputPW);

        DBHandler.getInstance();

        // Create SoundManager
        SoundEffects se = SoundEffects.get();
        SoundEffects.get().loadAll(this);

        // Client Debug 용
        if (Constant.DEBUG_SKIP_TITLE) {
            Intent intent = new Intent(this, GameActivity.class);
            startActivity(intent);
        }
    }

    public void onBtnSignIn(View view) {
        strID = editTextID.getText().toString();
        strPW = editTextPW.getText().toString();

        if (strID.length() == 0 || strPW.length() == 0)
            return;

        new Thread(new Runnable() {
            @Override
            public void run() {
                final boolean isSignIn = DBHandler.getInstance().SignIn(strID, strPW);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (isSignIn) {
                            SoundEffects.get().playSE(R.raw.login);
                            Intent intent = new Intent(TitleActivity.this, GameActivity.class);
                            ArrayList<Integer> highScoreAndRank = new ArrayList<>();
                            highScoreAndRank.add(DBHandler.getInstance().getHighScore());
                            highScoreAndRank.add(DBHandler.getInstance().getRank());
                            intent.putIntegerArrayListExtra("highScoreAndRank", highScoreAndRank);
                            startActivity(intent);
                        } else {
                            Toast.makeText(TitleActivity.this, "SignIn Fail!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    public void onBtnSignUp(View view) {
        // EditText에 입력한 정보로 회원가입하기
        strID = editTextID.getText().toString();
        strPW = editTextPW.getText().toString();

        if (strID.length() == 0 || strPW.length() == 0)
            return;

        SoundEffects.get().playSE(R.raw.button_click);
        new Thread(new Runnable() {
            @Override
            public void run() {
                final int resCode = DBHandler.getInstance().SignUp(strID, strPW);
                Log.d(TAG, "result code = " + resCode);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        switch (resCode) {
                            case 0: // 회원가입 성공
                                Toast.makeText(TitleActivity.this, "SignUp Success!", Toast.LENGTH_SHORT).show();
                                break;
                            case 1: // 회원가입 실패 - 중복 아이디 존재
                                Toast.makeText(TitleActivity.this, "SignUp Fail! - Input ID is already exist!", Toast.LENGTH_SHORT).show();
                                break;
                            case -1: // 회원가입 실패 - 커넥션 로스트
                                Toast.makeText(TitleActivity.this, "SignUp Fail! - Server Connection lost!", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
            }
        }).start();
    }
}