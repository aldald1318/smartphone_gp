package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj;

import android.graphics.Canvas;
import android.graphics.RectF;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.bitmap.FrameAnimationBitmap;

public class AnimObject extends GameObject {
    private static final String TAG = AnimObject.class.getSimpleName();
    protected FrameAnimationBitmap idleFab;
    protected final RectF dstRect;
    protected int width, height;
    protected  boolean IsRotateDraw = false;


    public AnimObject(float x, float y, int width, int height, int resId, int fps, int count) {
        idleFab = new FrameAnimationBitmap(resId, fps, count);
        this.x = x;
        this.y = y;
        this.dstRect = new RectF();
        if (width == 0) {
            width = UiBridge.x(idleFab.getWidth());
        } else if (width < 0) {
            width = UiBridge.x(idleFab.getWidth()) * -width / 100;
        }
        this.width = width;
        if (height == 0) {
            height = UiBridge.x(idleFab.getHeight());
        } else if (height < 0) {
            height = UiBridge.x(idleFab.getHeight()) * -height / 100;
        }
        this.height = height;
    }

    @Override
    public float getRadius() {
        return this.width / 2;
    }

    @Override
    public void draw(Canvas canvas) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        if(!IsRotateDraw)
        {
            dstRect.left = x - halfWidth;
            dstRect.top = y - halfHeight;
            dstRect.right = x + halfWidth;
            dstRect.bottom = y + halfHeight;
        }
        else
        {
            dstRect.left = x + halfWidth;
            dstRect.top = y - halfHeight;
            dstRect.right = x - halfWidth;
            dstRect.bottom = y + halfHeight;
        }

        idleFab.draw(canvas, dstRect, null);
    }

    public void onRotateDraw()
    {
        IsRotateDraw = true;
    }

}
