package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.database;

public class UserScoreInfo {
    private final String id;
    private final int highScore;
    private final int rank;

    UserScoreInfo(String id, int highScore, int rank){
        this.id = id;
        this.highScore = highScore;
        this.rank = rank;
    }

    public String getId(){return id.toString();}
    public int getHighScore(){return highScore;}
    public int getRank(){return rank;}
}
