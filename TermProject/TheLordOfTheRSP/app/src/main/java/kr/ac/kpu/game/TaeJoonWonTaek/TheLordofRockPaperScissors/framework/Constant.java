package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework;

import android.content.Context;

public class Constant {
    // 정상적인 게임 루트시 모두 false
    public static boolean DEBUG_SKIP_TITLE = false;
    public static boolean DEBUG_SKIP_LOBBY = false;

    public static Context currentContext;
}
