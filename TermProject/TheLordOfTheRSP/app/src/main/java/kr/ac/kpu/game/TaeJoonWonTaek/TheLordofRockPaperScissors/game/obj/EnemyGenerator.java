package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameTimer;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameWorld;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.GameplayScene;

public class EnemyGenerator {
    private static final String TAG = EnemyGenerator.class.getSimpleName();
    private final GameWorld gameWorld;
    private final Player player;

    private static final long INITIAL_GENERATE_INTERVAL = 5_000_000_000L;
    private static final long MINIMUM_GENERATE_INTERVAL = 3_500_000_000L;
    private static final double INTERVAL_REDUCE_RATE_PER_WAVE = 0.80;
    private static final int MAX_SPEED = 150;
    private static final int MAX_LEVEL = Enemy.ENEMY_INFO.length;
    private final float enemyHeight;
    private long lastGenerated;
    private long generationInterval;
    private final Random rand;
    private int wave;

    public EnemyGenerator(GameWorld gameWorld, Player player, float enemyHeight){
        this.gameWorld = gameWorld;
        this.player = player;
        this.enemyHeight = enemyHeight;

        generationInterval = INITIAL_GENERATE_INTERVAL;
        rand = new Random();
    }

    public void update(){
        long now = GameTimer.getCurrentTimeNanos();
        if(lastGenerated == 0){
            lastGenerated = now;
            return;
        }
        long elapsed = now - lastGenerated;
        if (elapsed > generationInterval) {
            generateWave();
            lastGenerated = now;
        }
    }

    private void generateWave() {
        wave++;
        String msg = "Wave " + wave + " Generated: /";
        int level = generateEnemy();
        msg += level + "/";
        generationInterval *= INTERVAL_REDUCE_RATE_PER_WAVE;
        if (generationInterval < MINIMUM_GENERATE_INTERVAL) {
            //게임끝
            generationInterval = MINIMUM_GENERATE_INTERVAL;
        }
        msg += " Interval=" + (generationInterval / 1_000_000_000.0);
        Log.d(TAG, msg);
    }

    private int generateEnemy() {
        int speed = 100 + 10 * wave;
        if (speed > MAX_SPEED) {
            speed = MAX_SPEED;
        }
        Log.d(TAG,"speed: "+speed);
        // wave에 따라 나올 동물나누기

        int level = wave / 3;
        if (level < 1) {
            level = 1;
        }
        if (level > MAX_LEVEL) {
            level = MAX_LEVEL;
        }

        int randLevel = rand.nextInt(level) + 1 + (int)(level / 100);
        if (randLevel > MAX_LEVEL) {
            randLevel = MAX_LEVEL;
        }
        Log.d(TAG,"randLevel: "+ randLevel);

        Enemy e = Enemy.get(player,enemyHeight, randLevel,speed);
        gameWorld.add(GameplayScene.Layer.enemy.ordinal(), e);

        return level;
    }

    public int receivePlayerMessage(Player.CommandState commandState) {
        if (Player.CommandState.none == commandState || commandState == null) return -1;
        Log.d(TAG, "commandState: " + commandState.name());

        ArrayList<GameObject> enemies = GameplayScene.getTop().getGameWorld().findLayerObjects(GameplayScene.Layer.enemy.ordinal());
        if(enemies.size() == 0) return -1;
        if(enemies.get(0) == null) return -1;
        enemies.get(0).processMessage(commandState.ordinal());

        return -1;
    }

}
