package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface;

public interface Subject {
    public void attachObserver(Observer o);
    public void detachObserver(Observer o);
    public void notifyObserver();
}
