package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameTimer;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;

public class Command extends BitmapObject {
    private static final String TAG = Command.class.getSimpleName();
    private final int LIFE_TIME = 20;
    private final boolean isEnemy;
    private int totalTime = 0;
    private int commandState;

    public Command(int commandState ,float x, float y, int width, int height, int resId, boolean isEnemy) {
        super(x, y, width, height, resId);
        this.commandState = commandState;
        this.isEnemy = isEnemy;
    }

    @Override
    public void update() {
        if(!isEnemy) {
            totalTime += GameTimer.getTimeDiffSeconds() * 100;
            if (totalTime > LIFE_TIME) {
                totalTime = 0;
                this.remove();
            }
        }
    }

    public void updateEnemy(float speed){
        this.x -= speed;
    }

    public int getCommandState() {
        return commandState;
    }
}
