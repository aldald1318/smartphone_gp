package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import android.util.Log;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.GameplayScene;

public class CommandQueue {
    private static final String TAG = CommandQueue.class.getSimpleName();
    private final int COMMAND_SIZE = 50;
    private final int COMMANDQUEUE_COLUMN = 4;

    private Queue<Command> commandQueue = new LinkedList<>();
    private float x,y;
    private int commandCounts;
    private float width, height;

    public CommandQueue(int commandCounts, float x, float y){
        this.x=x;
        this.y=y;
        this.commandCounts = commandCounts;
        if(commandCounts > 4)
            this.width = COMMANDQUEUE_COLUMN * COMMAND_SIZE;
        else
            this.width = commandCounts * COMMAND_SIZE;
        this.height = commandCounts/COMMANDQUEUE_COLUMN * COMMAND_SIZE;

        Random random = new Random();
        Log.d(TAG,"commandCounts "+ commandCounts);
        int w=0;
        for(int i=0; i<commandCounts; ++i){
            int cmdState = random.nextInt(3)+1;
            int cmdResId = -1;
            switch (cmdState){
                case 1:
                    cmdResId = R.mipmap.enemy_pattern_rock;
                    break;
                case 2:
                    cmdResId = R.mipmap.enemy_pattern_scissors;
                    break;
                case 3:
                    cmdResId = R.mipmap.enemy_pattern_paper;
                    break;
            }
            if(cmdResId == -1) continue;

            Command cmd = new Command(cmdState,this.x + (int)(w*COMMAND_SIZE) - COMMAND_SIZE/2, this.y +40 +(i/4)*COMMAND_SIZE,COMMAND_SIZE,COMMAND_SIZE,cmdResId,true);
            cmd.add(GameplayScene.Layer.command.ordinal());
            add(cmd);

            w+=1;
            if(w >= 4)
                w =0;
        }
    }

    public void update(float speed) {
        for(Command cmd : commandQueue){
            cmd.updateEnemy(speed);
        }
    }

    public void add(Command cmd){
        if(cmd == null) return;
        commandQueue.add(cmd);
    }

    public Command peek(){
        // 삭제없이 요소를 읽어옴
        // 비어있으면 null 반환
        return commandQueue.peek();
    }

    public Command poll(){
        // 객체를 꺼내서 반환
        // 비어있으면 null 반환
        return commandQueue.poll();
    }

    public void clear(){
        for(Command cmd : commandQueue){
            cmd.remove();
        }
        commandQueue.clear();
    }

    public int size(){
        return commandQueue.size();
    }
}
