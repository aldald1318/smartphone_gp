package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface;

public interface Observer {
    public void update(Subject subject);
}
