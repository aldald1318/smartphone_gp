package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import android.graphics.Canvas;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.bg.ImageScrollBackground;

public class GameBackground extends ImageScrollBackground {
    private final float screenRate;

    public GameBackground(int resId, float screenRate, int speed) {
        super(resId, Orientation.horizontal, speed);
        srcRect.set(0, 0, sbmp.getWidth(), sbmp.getHeight());
        this.screenRate = screenRate;
    }

    @Override
    protected void drawHorizontal(Canvas canvas) {
        int left = 0;
        int top = 0;
        int right = UiBridge.metrics.size.x;
        int bottom = (int)(UiBridge.metrics.size.y * screenRate);
        int pageSize = sbmp.getWidth() * (bottom - top) / sbmp.getHeight();

        canvas.save();
        canvas.clipRect(left, top, right, bottom);

        float curr = scrollX % pageSize;
        if (curr > 0) curr -= pageSize;
        curr += left;
        while (curr < right) {
            dstRect.set(curr, top, curr + pageSize, bottom);
            curr += pageSize;
            canvas.drawBitmap(sbmp.getBitmap(), srcRect, dstRect, null);
        }
        canvas.restore();
    }
}
