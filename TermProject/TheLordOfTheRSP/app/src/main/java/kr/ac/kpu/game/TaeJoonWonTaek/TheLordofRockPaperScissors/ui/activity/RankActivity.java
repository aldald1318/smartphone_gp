package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.database.DBHandler;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.database.UserScoreInfo;

public class RankActivity extends AppCompatActivity {
    private static final String TAG = RankActivity.class.getSimpleName();
    private static final int LOWER_RANKERS = 5;
    private ListView rankListView;
    private ArrayList<UserScoreInfo> userScoreInfos;
    private String myId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        String id = DBHandler.getInstance().getID();
        int highScore = DBHandler.getInstance().getHighScore();
        myId = DBHandler.getInstance().getID();

        startDBUpdateHighScore(id, highScore);

        rankListView = findViewById(R.id.rankListView);
        rankListView.setAdapter(adapter);
    }

    private void startDBUpdateHighScore(final String id, final int highScore) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RankActivity.this.userScoreInfos = DBHandler.getInstance().UpdateHighscore(id, highScore);
                rankListView.post(new Runnable() {
                    @Override
                    public void run() {
                        int position;
                        adapter.notifyDataSetChanged();
                        for ( int i = 0 ; i < userScoreInfos.size(); ++i){
                            if ( userScoreInfos.get(i).getId().equals(myId)){
                                if ( userScoreInfos.size() - i < LOWER_RANKERS)
                                    position = i + (userScoreInfos.size() - i);
                                else
                                    position = i + LOWER_RANKERS;
                                rankListView.smoothScrollToPosition(position);
                                break;
                            }
                        }
                    }
                });
            }
        }).start();
    }

    private BaseAdapter adapter = new BaseAdapter() {
        @Override
        public int getCount() {
            if (userScoreInfos!= null)
                return userScoreInfos.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(view == null){
                LayoutInflater inflater = getLayoutInflater();
                view = inflater.inflate(R.layout.rank_item, null);
            }
            UserScoreInfo userScoreInfo = userScoreInfos.get(position);

            TextView idView = view.findViewById(R.id.IDTextView);
            TextView rankView = view.findViewById(R.id.rankTextView);
            TextView highScoreView = view.findViewById(R.id.highScoreTextView);

            String tmp = userScoreInfo.getId();
            if ( myId.equals(tmp) ) {
                view.setBackgroundResource(R.mipmap.myscore_bg);
            }else
                view.setBackgroundResource(R.mipmap.bg_btn);

            idView.setText(userScoreInfo.getId());
            rankView.setText(""+userScoreInfo.getRank());
            highScoreView.setText(""+userScoreInfo.getHighScore());

            return view;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
