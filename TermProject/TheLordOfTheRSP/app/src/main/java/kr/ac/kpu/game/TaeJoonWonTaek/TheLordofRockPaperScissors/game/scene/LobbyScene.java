package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene;

import android.content.Intent;
import android.graphics.RectF;
import android.util.Log;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.Constant;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameScene;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameTimer;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.AnimObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ui.Button;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.GameBackground;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.Player;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.UIBackground;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.ui.activity.RankActivity;

public class LobbyScene extends GameScene {
    private static final String TAG = LobbyScene.class.getSimpleName();

    private static RectF layoutGameView;
    private RectF layoutUiView;

    private GameTimer timer;
    private Player maskDude;
    private int randHeight;

    public enum Layer {
        bg, anim_obj, ui, COUNT
    }

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
        if (timer.done()) {
            timer.reset();
        }
    }

    @Override
    public void enter() {
        super.enter();
        initObjects();
        SoundEffects.get().playBGM(R.raw.sound_bgm_lobby, true);
    }

    @Override
    public void resume()
    {
        super.resume();
        // Start Title BGM
        SoundEffects.get().playBGM(R.raw.sound_bgm_lobby, true);
    }

    // 로비 장식용 오브젝트 배치하기
    void createAnimObj(int x, int resId, int fps, int count, int i)
    {
        int animObjSize = 130 + i*i*2;
        if(animObjSize < 130)
            animObjSize = 130;
        AnimObject animobj = new AnimObject(x,layoutGameView.bottom - animObjSize/2 - randHeight, animObjSize,animObjSize,
                resId,fps,count);
        animobj.onRotateDraw();
        gameWorld.add(Layer.anim_obj.ordinal(), animobj);
    }


    private void initObjects() {
        // dpi param
        float gameviewRate = 0.6f;
        layoutGameView = new RectF(0,0,UiBridge.metrics.size.x,UiBridge.metrics.size.y * gameviewRate);
        layoutUiView = new RectF(0,UiBridge.metrics.size.y * gameviewRate,UiBridge.metrics.size.x,UiBridge.metrics.size.y);

        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;

        // timer
        timer = new GameTimer(2, 1);

        // title
        BitmapObject lobbyTitle = new BitmapObject(cx, UiBridge.y(100),(int)(screenWidth*0.7f),  (int)(screenHeight*0.3f), R.mipmap.title_logo);
        gameWorld.add(GameOverScene.Layer.obj.ordinal(), lobbyTitle);

        int marginX = (int) (screenWidth * 0.15);
        // player
        int playerSize = 220;
        randHeight = (int) (layoutGameView.bottom * 0.185);
        maskDude =  new Player(screenWidth - marginX,layoutGameView.bottom - playerSize/2 - randHeight, playerSize,playerSize);
        maskDude.setIdleFab(R.mipmap.player_maskdude_run, 10, 12);
        gameWorld.add(Layer.anim_obj.ordinal(), maskDude);

        // Anims
        int increaseMarginX = (int) (marginX * 0.55f);
        int increaselevel = 1;

        marginX += increaseMarginX +120;
        createAnimObj(screenWidth - marginX,R.mipmap.chicken_run,10,14, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.rock_run,14,14, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.radish_run,12,12, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.trunk_run,14,14, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.mushroom_run,16,16, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.ghost_idle,10,10, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.angrypig_run,12,12, increaselevel);

        marginX += increaseMarginX;
        increaselevel += 1;
        createAnimObj(screenWidth - marginX,R.mipmap.bunny_run,12,12, increaselevel);

        // background
        GameBackground gamebg_3rdLayer = new GameBackground(R.mipmap.bg_3rd, gameviewRate, -5);
        gameWorld.add(GameplayScene.Layer.bg.ordinal(), gamebg_3rdLayer);
        GameBackground gamebg_2ndLayer = new GameBackground(R.mipmap.bg_2nd, gameviewRate, -15);
        gameWorld.add(GameplayScene.Layer.bg.ordinal(), gamebg_2ndLayer);
        GameBackground gamebg_1stLayer = new GameBackground(R.mipmap.bg_forest, gameviewRate, -25);
        gameWorld.add(GameplayScene.Layer.bg.ordinal(), gamebg_1stLayer);
        UIBackground uibg = new UIBackground(layoutUiView.right/2,layoutUiView.top + (layoutUiView.bottom - layoutUiView.top) * 0.5f,(int)layoutUiView.right + 30, (int) (layoutUiView.bottom - layoutUiView.top),R.mipmap.bg_ui);
        gameWorld.add(GameplayScene.Layer.bg.ordinal(), uibg);


        /////////////////////// Button
        int buttonSize = (int)(screenWidth * 0.38);
        int buttonY = (int) (layoutUiView.top + (layoutUiView.bottom - layoutUiView.top)*0.5f);
        int btnMarginX = (int) (screenWidth * 0.05f);
        // start game
        Button button = new Button(screenWidth/2 + buttonSize/2 + btnMarginX, buttonY,
                buttonSize,buttonSize, R.mipmap.btn_rockpaperscissors, R.mipmap.startgame_normal, R.mipmap.startgame_pressed);
        button.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                SoundEffects.get().stopBGM();
                GameplayScene scene = new GameplayScene();
                scene.push();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button);

        // ranking
        button = new Button(screenWidth/2 - buttonSize/2 - btnMarginX, buttonY, buttonSize,buttonSize, R.mipmap.btn_rockpaperscissors, R.mipmap.score_normal, R.mipmap.score_pressed);
        button.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                Intent intent = new Intent(Constant.currentContext, RankActivity.class);
                Constant.currentContext.startActivity(intent);
            }
        });
        gameWorld.add(Layer.ui.ordinal(),button);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBack");
        SoundEffects.get().stopBGM();
    }
}
