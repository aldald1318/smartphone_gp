package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.ui;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ui.Button;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.LifeObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.Potion;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.GameplayScene;

public class PotionButton extends Button {
    private static final String TAG = PotionButton.class.getSimpleName();
    private static final int POTION_PRICE = -5;

    private int currentPotionCount = 0;

    // components
    private Potion[] potions = new Potion[GameplayScene.MAX_COUNT_POTION];
    private LifeObject lifeObject;

    public PotionButton(LifeObject lifeObject, float x, float y, int width, int height, int resId, int bgNormalResId, int bgPressResId) {
        super(x, y, width, height, resId, bgNormalResId, bgPressResId);
        this.lifeObject = lifeObject;
    }

    public void SetPotions(Potion[] potions)
    {
        if(potions == null) return;
        currentPotionCount = GameplayScene.MAX_COUNT_POTION;
        this.potions = potions;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.d(TAG, "Down");
                break;
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "Up");
                // potion count 감소
                if(currentPotionCount <= 0 || lifeObject.getLife() >= lifeObject.PLAYER_LIFE) {
                    Log.d(TAG,"Max HP");
                    break;
                }

                currentPotionCount--;
                potions[currentPotionCount].SetActive(false);
                SoundEffects.get().playSE(R.raw.button_click);
                lifeObject.increaseLife();
                GameplayScene.get().buyPotion(POTION_PRICE);
                break;
        }
        return super.onTouchEvent(e);
    }
}
