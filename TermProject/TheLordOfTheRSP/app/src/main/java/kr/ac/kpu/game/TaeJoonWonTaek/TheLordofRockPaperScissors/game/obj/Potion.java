package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import android.graphics.Canvas;
import android.graphics.Point;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;

public class Potion extends BitmapObject {
    private static final String TAG = Point.class.getSimpleName();
    private boolean isActive = true;

    public Potion(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
    }

    @Override
    public void draw(Canvas canvas) {
        if(isActive) super.draw(canvas);
    }

    public void SetActive(boolean isAct){
        if(isActive == isAct) return;
        isActive = isAct;
    }
}
