package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface;

public interface Recyclable {
    public void recycle();
}
