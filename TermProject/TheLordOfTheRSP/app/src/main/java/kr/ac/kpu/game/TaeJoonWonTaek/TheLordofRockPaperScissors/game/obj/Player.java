package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import android.graphics.Canvas;
import android.graphics.RectF;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface.BoxCollidable;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.AnimObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.GameplayScene;

public class Player extends AnimObject implements BoxCollidable {
    private static final String TAG = Player.class.getSimpleName();
    protected FrameAnimationBitmap runFab = null;
    protected FrameAnimationBitmap hitFab = null;

    @Override
    public void getBox(RectF rect) {
        int hw = width /2;
        int hh = height/2;
        rect.left = x - hw;
        rect.right = x + hw;
        rect.top = y - hh;
        rect.bottom = y + hh;
    }

    public enum MoveState {
        idle, run, hit
    }
    public enum CommandState{
        none, rock,scissors,paper
    }
    protected MoveState moveState = MoveState.idle;
    protected CommandState commandState = CommandState.none;

    public void setIdleFab(int resId, int fps,int count)
    {
        idleFab = new FrameAnimationBitmap(resId,fps,count);
    }

    public Player(float x, float y, int width, int height) {
        super(x, y, width, height, R.mipmap.player_maskdude_idle, 10, 11);
        runFab = new FrameAnimationBitmap(R.mipmap.player_maskdude_run,10,12);
        hitFab = new FrameAnimationBitmap(R.mipmap.player_maskdude_hit,10,7);
    }

    @Override
    public void update() {
        if(moveState == MoveState.run){
            boolean done = runFab.done();
            if(done){
                moveState = MoveState.idle;
                idleFab.reset();
            }
        }else if(moveState == MoveState.hit){
            boolean done = hitFab.done();
            if(done){
                moveState = MoveState.idle;
                idleFab.reset();
            }
        }
    }

    @Override
    public void draw(Canvas canvas) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        dstRect.left = x - halfWidth;
        dstRect.top = y - halfHeight;
        dstRect.right = x + halfWidth;
        dstRect.bottom = y + halfHeight;

        if(moveState == MoveState.idle){
            idleFab.draw(canvas, dstRect, null);
        }else{
            canvas.save();
            if(moveState == MoveState.run){
                runFab.draw(canvas,dstRect,null);
            }else if(moveState == MoveState.hit){
                hitFab.draw(canvas,dstRect,null);
            }
            canvas.restore();
        }
    }

    public void setMoveState(MoveState moveState){
        this.moveState = moveState;

        if(moveState == MoveState.run)
            runFab.reset();
        if(moveState == MoveState.hit) {
            hitFab.reset();
        }
    }

    public void setCommandState(CommandState commandState){
        this.commandState = commandState;
        if(commandState == CommandState.none) return;

        Command command = null;
        int cmdMargin = 15;
        int cmdSize = 100;
        if(commandState == CommandState.rock)
            command = new Command(commandState.ordinal(),x + width/2 + cmdMargin,y - height/2 - cmdMargin,cmdSize,cmdSize,R.mipmap.player_pressed_pattern_rock, false);
        if(commandState == CommandState.scissors)
            command = new Command(commandState.ordinal(),x + width/2 + cmdMargin,y - height/2 - cmdMargin,cmdSize,cmdSize,R.mipmap.player_pressed_pattern_scissors, false);
        if(commandState == CommandState.paper)
            command = new Command(commandState.ordinal(),x + width/2 + cmdMargin,y - height/2 - cmdMargin,cmdSize,cmdSize,R.mipmap.player_pressed_pattern_paper, false);

        if(command != null)
            command.add(GameplayScene.Layer.command.ordinal());
    }

}
