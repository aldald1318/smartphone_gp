package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface;

import android.graphics.RectF;

public interface BoxCollidable {
    public void getBox(RectF rect);
}
