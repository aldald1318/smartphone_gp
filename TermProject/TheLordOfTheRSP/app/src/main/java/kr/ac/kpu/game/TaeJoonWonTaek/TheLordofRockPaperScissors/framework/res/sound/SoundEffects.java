package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import java.util.HashMap;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.Constant;

public class SoundEffects {
    private static final String TAG = SoundEffects.class.getSimpleName();
    private static SoundEffects singleton;

    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundIdMap = new HashMap<>();
    private MediaPlayer currentMediaPlayer;
    private static final int[] SOUND_IDS = {
            R.raw.sound_bgm_lobby,
            R.raw.sound_bgm_gameplay,
            R.raw.hit,
            R.raw.bad,
            R.raw.good,
            R.raw.perfect,
            R.raw.login,
            R.raw.button_click,
    };

    private Context currContext;

    public static SoundEffects get() {
        if (singleton == null) {
            singleton = new SoundEffects();
        }
        return singleton;
    }
    private SoundEffects() {
        AudioAttributes audioAttributes;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            this.soundPool = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(5) //동시에 재생가능한 음원수
                    .build();
        } else {
            this.soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        }
    }
    public void loadAll(Context context) {
        currContext = context;

        for (int resId: SOUND_IDS) {
            int soundId = soundPool.load(context, resId, 0);
            soundIdMap.put(resId, soundId);
        }
    }

    public int playSE(int resId) {
        int soundId = soundIdMap.get(resId);
        int streamId = soundPool.play(soundId, 1f, 1f, 0, 0, 1f);
        return streamId;
    }

    public void playBGM(int resId, boolean isLoop){
        if(currentMediaPlayer != null)
        {
            currentMediaPlayer.stop();
            currentMediaPlayer = null;
        }
        currentMediaPlayer = MediaPlayer.create(Constant.currentContext,resId);
        currentMediaPlayer.setLooping(isLoop);
        currentMediaPlayer.start();
    }

    public void pauseStartBGM()
    {
        currentMediaPlayer.start();
    }

    public void pauseBGM()
    {
        currentMediaPlayer.pause();
    }



    public void stopBGM() {
        currentMediaPlayer.stop();
    }
}
