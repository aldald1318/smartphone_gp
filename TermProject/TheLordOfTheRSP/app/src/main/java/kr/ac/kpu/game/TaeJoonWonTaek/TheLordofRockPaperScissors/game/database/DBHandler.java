package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.database;

import android.util.Log;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

public class DBHandler {
    private static final String TAG = DBHandler.class.getSimpleName();
    private static final int INPUT_ID_ALREADY_EXISTS = 1;
    private static final int SIGNUP_SUCCESS = 0;
    private static final int SIGNUP_FAIL_CONNECTION_LOST = -1;
    protected static DBHandler singleton;
    private Connection connection;
    private String ID = null;
    private int highScore;
    private int rank;
    private CallableStatement cstmt;

    private DBHandler(){initialize();}

    private void initialize() {
        tryConnection();
    }

    public static DBHandler getInstance(){
        if ( singleton == null) singleton = new DBHandler();
        return singleton;
    }

    public String getID() {return this.ID;}
    public int getHighScore(){return highScore;}
    public int getRank(){return rank;}
    public void setHighScore(int highScore){
        if ( this.highScore < highScore)
            this.highScore = highScore;
    }

    private boolean tryConnection() {
        try {
            if (connection != null && !connection.isClosed())
                return true;

            String dbIp = "thelordofrockpaperscissors.c5m8ynryckyd.ap-northeast-2.rds.amazonaws.com";
            String dbName = "TheLordofRPS";
            String dbUser = "admin";
            String dbUserPass = "rkdnlqkdnlqh";
            ConnectionClass connClass = new ConnectionClass();
            connection = connClass.getConnection(dbUser, dbUserPass, dbName, dbIp);
            if (connection == null) {
                Log.d(TAG, connClass.getLastErrMsg());
                return false;
            } else {
                if (connection.isClosed()) {
                    Log.d(TAG, "DataBase 연결 실패.");
                    return false;
                } else {
                        Log.d(TAG, "DataBase 연결에 성공했습니다.");
                    return true;
                }
            }
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    // 로그인
    public boolean SignIn(String id, String password){
        Log.d(TAG, "SignIn Called!");
        // SQL Server 연결상태 확인
        if ( tryConnection()){
            try {
                cstmt = connection.prepareCall("{? = call dbo.SignIn(?, ?, ?, ?)}");
                cstmt.registerOutParameter(1, Types.INTEGER);
                cstmt.setString("ID_STR", id);
                cstmt.setString("Password", password);
                cstmt.registerOutParameter("HighScore", Types.INTEGER);
                cstmt.registerOutParameter("Rank", Types.INTEGER);
                cstmt.execute();

                int isExist = cstmt.getInt(1);
                Log.d(TAG, "ID 존재함! " + isExist );
                if (isExist == 0){
                    Log.d(TAG, "ID가 틀립니다. 혹은 해당 아이디가 존재하지 않습니다.");
                    return false;
                }
                else {
                    ID = id;
                    highScore = cstmt.getInt("HighScore");
                    rank = cstmt.getInt("Rank");
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    // 회원가입
    public int SignUp(String id, String password){
        Log.d(TAG, "SignUp Called!");
        // SQL Server 연결상태 확인
        if ( tryConnection()){
            try{
                cstmt = connection.prepareCall("? = call dbo.CheckIDExist(?)");
                cstmt.registerOutParameter(1, Types.INTEGER);
                cstmt.setString("ID_STR", id);
                cstmt.execute();
                if (cstmt.getInt(1) == 1){
                    // 중복된 아이디 존재!
                    return INPUT_ID_ALREADY_EXISTS;
                }
                cstmt = connection.prepareCall("call dbo.SignUp(?, ?)");
                cstmt.setString("ID_STR", id);
                cstmt.setString("Password", password);
                cstmt.execute();
                Log.d(TAG, "회원가입완료.");
                return SIGNUP_SUCCESS;

            }catch (SQLException e){
                e.printStackTrace();;
            }
        }
        return SIGNUP_FAIL_CONNECTION_LOST;
    }

    // 하이스코어 업데이트, 랭킹 가져오기
    public ArrayList<UserScoreInfo> UpdateHighscore(String id, int highScore){
        Log.d(TAG, "UpdateHighscore Called!");
        ArrayList<UserScoreInfo> userScoreInfos = new ArrayList<>();

        // SQL Server 연결상태 확인
        if ( tryConnection()) {
            try {
                PreparedStatement pstmt = connection.prepareStatement("{call dbo.UpdateHighscore(?, ?)}");
                pstmt.setString(1, id);
                pstmt.setInt(2, highScore);
                ResultSet rs = pstmt.executeQuery();

                String queryID;
                int queryHighScore;
                int queryRank;
                while(rs.next()){
                    queryID = rs.getString("ID");
                    queryID = queryID.replaceAll(" ", "");

                    queryHighScore = rs.getInt("HighScore");
                    queryRank = rs.getInt("MyRank");
                    userScoreInfos.add(new UserScoreInfo(queryID, queryHighScore, queryRank));
                    Log.d(TAG, "Queried ID = " + queryID + ", HighScore = " + queryHighScore + ", Rank = " + queryRank );
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userScoreInfos;
    }
}
