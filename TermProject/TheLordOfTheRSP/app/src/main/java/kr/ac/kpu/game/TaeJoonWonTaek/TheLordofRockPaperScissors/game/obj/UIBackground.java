package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;

public class UIBackground extends BitmapObject {

    public UIBackground(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
    }
}
