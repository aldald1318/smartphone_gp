package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.ui;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ui.Button;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.Player;

public class ScissorsCommand extends Button {
    private static final String TAG = ScissorsCommand.class.getSimpleName();
    private Player player;

    public ScissorsCommand(Player player, float x, float y, int width, int height, int resId, int bgNormalResId, int bgPressResId) {
        super(x, y, width, height, resId, bgNormalResId, bgPressResId);
        this.player = player;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (this.bgNormal.getBounds().contains((int)e.getX(), (int)e.getY())) {
                    Log.d(TAG, "Down");
                    player.setMoveState(Player.MoveState.run);
                    player.setCommandState(Player.CommandState.scissors);
                    // parse to moster state
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "Up");
                break;
        }
        return super.onTouchEvent(e);
    }
}