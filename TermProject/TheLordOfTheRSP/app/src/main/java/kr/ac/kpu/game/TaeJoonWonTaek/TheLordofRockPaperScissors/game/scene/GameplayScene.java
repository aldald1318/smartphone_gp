package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene;

import android.graphics.RectF;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameScene;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameTimer;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ScoreObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ui.Button;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.database.DBHandler;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.EnemyGenerator;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.GameBackground;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.LifeObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.Player;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.Potion;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj.UIBackground;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.ui.PaperCommand;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.ui.PotionButton;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.ui.RockCommand;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.ui.ScissorsCommand;

public class GameplayScene extends GameScene{
    private static final String TAG = GameplayScene.class.getSimpleName();
    public static final int MAX_COUNT_POTION = 3;

    private static RectF layoutGameView;
    private RectF layoutUiView;

    public static boolean isPause = false;

    private ScoreObject scoreObject;
    private ScoreObject goldObject;
    private GameTimer timer;
    private Player maskDude;
    private int randHeight;
    private EnemyGenerator enemyGenerator;
    private LifeObject lifeObject;
    private BitmapObject goldBg;
    private Potion[] potions = new Potion[MAX_COUNT_POTION];

    private static GameplayScene instance;

    public enum Layer {
        bg, enemy, command,player, ui, COUNT
    }

    public static GameplayScene get() {
        return instance;
    }

    public static RectF getLayoutGameView(){
        return layoutGameView;
    }

    @Override
    protected int getLayerCount() {
        return GameplayScene.Layer.COUNT.ordinal();
    }

    @Override
    public void addScore(int score){
        scoreObject.add(score);
    }

    @Override
    public void addGold(int gold) {goldObject.add(gold);}
    public void buyPotion(int gold){
        goldObject.add(gold);
    }

    public void restart() {
        lifeObject.reset();
        scoreObject.reset();
        goldObject.reset();
        // potion reset
        for (int layer = Layer.enemy.ordinal(); layer <= Layer.command.ordinal(); layer++) {
            gameWorld.removeAllObjectsAt(layer);
        }
    }

    public void decreaseLife()
    {
        int life = lifeObject.decreaseLife();
        if (life == 0) {
            GameOverScene scene = new GameOverScene();
            DBHandler.getInstance().setHighScore(scoreObject.getScoreValue());
            scene.push();
        }
    }

    @Override
    public void update() {
        super.update();

        if (timer.done()) {
            scoreObject.add(1);
//            goldObject.add(1);
            timer.reset();
        }

        enemyGenerator.update();
    }

    @Override
    public void enter() {
        super.enter();
        instance = this;
        initObjects();
        // Start Title BGM
        SoundEffects.get().playBGM(R.raw.sound_bgm_gameplay, true);
    }

    @Override
    public void resume()
    {
        super.resume();
        // Start Title BGM
        if(!isPause)
            SoundEffects.get().playBGM(R.raw.sound_bgm_gameplay, true);
        else {
            isPause = false;
            SoundEffects.get().pauseStartBGM();
        }
    }

    private void initObjects() {
        // dpi param
        float gameviewRate = 0.6f;
        layoutGameView = new RectF(0,0,UiBridge.metrics.size.x,UiBridge.metrics.size.y * gameviewRate);
        layoutUiView = new RectF(0,UiBridge.metrics.size.y * gameviewRate,UiBridge.metrics.size.x,UiBridge.metrics.size.y);

        int cx = UiBridge.metrics.center.x;
        int cy = UiBridge.metrics.center.y;
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;

        // timer
        timer = new GameTimer(2, 1);

        // player
        int playerSize = 220;
        randHeight = (int) (layoutGameView.bottom * 0.185);
        maskDude =  new Player(playerSize/2,layoutGameView.bottom - playerSize/2 - randHeight, playerSize,playerSize);
        gameWorld.add(Layer.player.ordinal(), maskDude);

        // enemy
        enemyGenerator = new EnemyGenerator(gameWorld,maskDude, layoutGameView.bottom - randHeight);

        // background
        GameBackground gamebg_3rdLayer = new GameBackground(R.mipmap.bg_3rd, gameviewRate, -5);
        gameWorld.add(Layer.bg.ordinal(), gamebg_3rdLayer);
        GameBackground gamebg_2ndLayer = new GameBackground(R.mipmap.bg_2nd, gameviewRate, -15);
        gameWorld.add(Layer.bg.ordinal(), gamebg_2ndLayer);
        GameBackground gamebg_1stLayer = new GameBackground(R.mipmap.bg_forest, gameviewRate, -25);
        gameWorld.add(Layer.bg.ordinal(), gamebg_1stLayer);
        UIBackground uibg = new UIBackground(layoutUiView.right/2,layoutUiView.top + (layoutUiView.bottom - layoutUiView.top) * 0.5f,(int)layoutUiView.right + 30, (int) (layoutUiView.bottom - layoutUiView.top),R.mipmap.bg_ui);
        gameWorld.add(Layer.bg.ordinal(), uibg);

        // ui (button)
        // scissors / rock / paper
        int btnMargin = 40;
        int btnWidth = screenWidth/3 - btnMargin*2;
        int btnHeight = btnWidth;
        int btnY = screenHeight - btnHeight/2 - btnMargin;
        ScissorsCommand btnScissors = new ScissorsCommand(
                maskDude,
                0 + btnWidth/2 + btnMargin, btnY, btnWidth, btnHeight,
                R.mipmap.btn_rockpaperscissors,R.mipmap.scissors_normal,R.mipmap.scissors_pressed);
        RockCommand btnRock = new RockCommand(
                maskDude,
                cx,btnY, btnWidth, btnHeight,
                R.mipmap.btn_rockpaperscissors,R.mipmap.rock_normal,R.mipmap.rock_pressed);
        PaperCommand btnPaper = new PaperCommand(
                maskDude,
                screenWidth - btnWidth/2 - btnMargin,btnY, btnWidth, btnHeight,
                R.mipmap.btn_rockpaperscissors,R.mipmap.paper_normal,R.mipmap.paper_pressed);
        btnScissors.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                enemyGenerator.receivePlayerMessage(Player.CommandState.scissors);
            }
        });
        btnRock.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                enemyGenerator.receivePlayerMessage(Player.CommandState.rock);
            }
        });
        btnPaper.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                enemyGenerator.receivePlayerMessage(Player.CommandState.paper);
            }
        });

        gameWorld.add(Layer.ui.ordinal(), btnScissors);
        gameWorld.add(Layer.ui.ordinal(), btnRock);
        gameWorld.add(Layer.ui.ordinal(), btnPaper);

        // Params
        // hp-potion
        int potionWidth = (int) (screenWidth*0.4);
        int potionHeight = btnHeight;
        int potionMarginX = btnMargin + 20;
        int potionMarginY = 10;
        int potionY = (int) (layoutUiView.top + btnHeight/2 + btnMargin);

        // ui(imageview)
        int uiTopMargin = 20;
        int uiSideMargin = 10;
        int hpSize = 25;
        int settingSize = 90;
        int scoreSize = 25;
        int goldSize = 40;

        // hp bar
        RectF lbox = new RectF(UiBridge.x(uiSideMargin), UiBridge.y(uiTopMargin), UiBridge.x(uiSideMargin + hpSize), UiBridge.y(uiTopMargin+ hpSize));
        lifeObject = new LifeObject(potions,R.mipmap.hearts, lbox, LifeObject.PLAYER_LIFE);
        gameWorld.add(Layer.ui.ordinal(), lifeObject);

        final PotionButton hpPotion = new PotionButton(lifeObject,
                layoutUiView.left + potionWidth/2 + potionMarginX, potionY + potionMarginY, potionWidth, potionHeight,
                R.mipmap.btn_rockpaperscissors,R.mipmap.potion_normal,R.mipmap.potion_pressed);


        int potionCompMarginY = 60;
        potions[0] = new Potion(layoutUiView.left + potionWidth/2 + potionMarginX - potionWidth/3,potionY + potionMarginY - potionCompMarginY,120,120, R.mipmap.hp_potion);
        potions[1] = new Potion(layoutUiView.left + potionWidth/2 + potionMarginX,potionY + potionMarginY - potionCompMarginY,120,120, R.mipmap.hp_potion);
        potions[2] = new Potion(layoutUiView.left + potionWidth/2 + potionMarginX + potionWidth/3,potionY + potionMarginY - potionCompMarginY,120,120, R.mipmap.hp_potion);
        hpPotion.SetPotions(potions);

        // Add Potions -
        hpPotion.add(GameplayScene.Layer.ui.ordinal());
        potions[0].add(GameplayScene.Layer.ui.ordinal());
        potions[1].add(GameplayScene.Layer.ui.ordinal());
        potions[2].add(GameplayScene.Layer.ui.ordinal());

        // score
        RectF rbox = new RectF(UiBridge.x((int) (- (uiSideMargin+ scoreSize*0.8))), UiBridge.y(uiTopMargin), UiBridge.x(-uiSideMargin), UiBridge.y(uiTopMargin+ scoreSize));
        scoreObject = new ScoreObject(R.mipmap.number_64x84, rbox);
        gameWorld.add(Layer.ui.ordinal(), scoreObject);
        // setting
        Button btnSettings = new Button( cx + settingSize/2, UiBridge.y(uiTopMargin + 5)+ settingSize/2,settingSize,settingSize, R.mipmap.btn_settings, R.mipmap.blue_round_btn, R.mipmap.blue_round_btn);
        btnSettings.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                SoundEffects.get().pauseBGM();
                DialogScene scene = new DialogScene();
                scene.push();
                return;
            }
        });
        gameWorld.add(GameplayScene.Layer.ui.ordinal(), btnSettings);
        // gold BG
        int goldBgWidth = 350;
        int goldBgHeight = 60;
        goldBg = new BitmapObject( goldBgWidth/2,layoutUiView.top - goldBgHeight/2,goldBgWidth,goldBgHeight,R.mipmap.ui_gold);
        goldBg.add(Layer.ui.ordinal());

        // gold
        int goldMarginSide = 40;
        int goldMarginTop = 10;
        RectF goldbox = new RectF(goldBgWidth - goldSize - goldMarginSide, layoutUiView.top - goldSize - goldMarginTop,
                goldBgWidth - goldMarginSide,layoutUiView.top- goldMarginTop);
        goldObject = new ScoreObject(R.mipmap.number_64x84_gold, goldbox);
        gameWorld.add(Layer.ui.ordinal(), goldObject);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SoundEffects.get().stopBGM();
    }
}
