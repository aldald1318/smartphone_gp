package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameTimer;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;

public class Feedback extends BitmapObject {
    private final int LIFE_TIME = 50;
    private int totalTime = 0;

    public Feedback(float x, float y, int width, int height, int resId) {
        super(x, y, width, height, resId);
    }

    @Override
    public void update() {
        totalTime += GameTimer.getTimeDiffSeconds() *100;
        if(totalTime > LIFE_TIME){
            totalTime =0;
            this.remove();
        }
    }
}
