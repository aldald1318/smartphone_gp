package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.Constant;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.view.GameView;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.GameplayScene;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.LobbyScene;

public class GameActivity extends AppCompatActivity {
    private static final long BACKKEY_INTERVAL_MSEC = 1000;
    private static final String TAG = GameActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        UiBridge.setActivity(this);
        super.onCreate(savedInstanceState);
        setContentView(new GameView(this));
        Constant.currentContext = this;

        if(Constant.DEBUG_SKIP_LOBBY)
        {
            new GameplayScene().run();
        }
        else
            new LobbyScene().run();
    }

    private long lastBackPressedOn;
    @Override
    public void onBackPressed() {
        long now = System.currentTimeMillis();
        long elapsed = now - lastBackPressedOn;
        if (elapsed <= BACKKEY_INTERVAL_MSEC) {
            handleBackPressed();
            return;
        }
        Log.d("BackKey", "elapsed="+elapsed);
        Toast.makeText(this,
                "Press Back key twice quickly to exit",
                Toast.LENGTH_SHORT)
                .show();
        lastBackPressedOn = now;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void handleBackPressed() {
//        SoundEffects.get().stopBGMAll();
        GameplayScene.getTop().onBackPressed();
    }
}
