package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.obj;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.Log;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface.BoxCollidable;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface.Recyclable;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameTimer;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.AnimObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.util.CollisionHelper;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene.GameplayScene;

public class Enemy extends AnimObject implements BoxCollidable, Recyclable {
    private static final String TAG = Enemy.class.getSimpleName();
    private Player player;
    private FrameAnimationBitmap hitFab = null;
    private CommandQueue commandQueue;
    private boolean dieStart = false;
    private int speed;
    private int level;
    private int damage;
    private int score;

    public static class EnemyInfo{
        public int idleResId;
        public int idleFPS;
        public int idleFrameCount;
        public int hitResId;
        public int hitFPS;
        public int hitFrameCount;
        public int width;
        public int height;
        public int size;
        public EnemyInfo(int idleResId,int idleFPS, int idleFrameCount,
                         int hitResId, int hitFPS, int hitFrameCount, int width, int height){
            this.idleResId = idleResId;
            this.idleFPS = idleFPS;
            this.idleFrameCount = idleFrameCount;
            this.hitResId = hitResId;
            this.hitFPS = hitFPS;
            this.hitFrameCount = hitFrameCount;
            this.width = width;
            this.height = height;
        }
    }

    public enum MoveState {
        run, hit
    }
    protected MoveState moveState = MoveState.run;

    public static EnemyInfo[] ENEMY_INFO = {
            new EnemyInfo(R.mipmap.chicken_run,10,14,R.mipmap.chicken_hit,5,5, 130,130),
            new EnemyInfo(R.mipmap.bat_flying,7,7,R.mipmap.bat_hit,5,5, 190,130),
            new EnemyInfo(R.mipmap.snail_run,10,10,R.mipmap.snail_hit,5,5, 180,130),
            new EnemyInfo(R.mipmap.rock_run,14,14,R.mipmap.rock_hit,1,1, 130,130),
            new EnemyInfo(R.mipmap.rino_run,10,6,R.mipmap.rino_hit,5,5, 190,130),
            new EnemyInfo(R.mipmap.radish_run,12,12,R.mipmap.radish_hit,5,5, 130,180),
            new EnemyInfo(R.mipmap.trunk_run,14,14,R.mipmap.trunk_hit,5,5, 200,180),
            new EnemyInfo(R.mipmap.mushroom_run,16,16,R.mipmap.mushroom_hit,5,5, 150,150),
            new EnemyInfo(R.mipmap.ghost_idle,10,10,R.mipmap.ghost_hit,5,5, 130,150),
            new EnemyInfo(R.mipmap.chameleon_run,8,8,R.mipmap.chameleon_hit,5,5, 180,150),
            new EnemyInfo(R.mipmap.blueb_fly,9,9,R.mipmap.blueb_hit,5,5, 150,130),
            new EnemyInfo(R.mipmap.angrypig_run,12,12,R.mipmap.angrypig_hit,5,5, 250,200),
            new EnemyInfo(R.mipmap.bunny_run,12,12,R.mipmap.bunny_hit,5,5, 260,360),
    };

    public static Enemy get(Player player,float y, int level, int speed) {
//        Log.d(TAG,"get()");
        level--;
        if (level >= ENEMY_INFO.length) {
            level = ENEMY_INFO.length - 1;
        }

        EnemyInfo enemyInfo = ENEMY_INFO[level];
        Enemy e = (Enemy) GameplayScene.getTop().getGameWorld().getRecyclePool().get(Enemy.class);
        if(e == null){
            Log.d(TAG,"create");
            e = new Enemy(player, UiBridge.metrics.size.x,y - enemyInfo.height/2 ,enemyInfo.width,enemyInfo.height, speed, level,
                    enemyInfo.idleResId,enemyInfo.idleFPS,enemyInfo.idleFrameCount,enemyInfo.hitResId,enemyInfo.hitFPS,enemyInfo.hitFrameCount);
        }
        else{
            e.player = player;
            e.x = UiBridge.metrics.size.x;
            e.y = y - enemyInfo.height/2;
            e.width = enemyInfo.width;
            e.height = enemyInfo.height;
            e.speed = speed;
            e.level = level;
            e.idleFab = new FrameAnimationBitmap(enemyInfo.idleResId,enemyInfo.idleFPS,enemyInfo.idleFrameCount);
            e.hitFab = new FrameAnimationBitmap(enemyInfo.hitResId,enemyInfo.hitFPS,enemyInfo.hitFrameCount);
        }
        e.damage = (level+1) *10;
        e.score = (level+1) * 10;

        // Create Commands & CommandQueue
        e.commandQueue = new CommandQueue((level+1), e.x, e.y+e.height/2);
        return e;
    }

    private Enemy(Player player, float x, float y, int width, int height, int speed, int level,
                  int idleResId, int idleFps, int idleCount,
                  int hitResId, int hitFps, int hitCount) {
        super(x, y, width, height, idleResId, idleFps, idleCount);
        this.hitFab = new FrameAnimationBitmap(hitResId,hitFps,hitCount);
        this.player = player;
        this.speed = speed;
        this.level = level;
    }

    public void reset(){
        moveState = MoveState.run;
        idleFab.reset();
        hitFab.reset();
        commandQueue.clear();
        dieStart = false;
    }

    @Override
    public void update() {
        // 충돌
        if(CollisionHelper.collides(player,this)){
            if(!dieStart)
            {
                this.moveState = MoveState.hit;
                hitFab.reset();
                player.setMoveState(Player.MoveState.hit);
                SoundEffects.get().playSE(R.raw.hit);
                dieStart = true;
            }

            boolean done = hitFab.done();
            if(done){
                Log.d(TAG, "die()");
                // player hp 감소
                GameplayScene.get().decreaseLife();
                reset();
                this.remove();
            }
        }else{
            if(commandQueue.size() <= 0) {
                // 스코어 증가
                GameplayScene.getTop().addScore(score);
                // 골드증가
                GameplayScene.get().addGold(level+1);
                reset();
                remove();
            }

            x -= GameTimer.getTimeDiffSeconds()*speed;
            commandQueue.update(GameTimer.getTimeDiffSeconds()*speed);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        float halfWidth = width / 2;
        float halfHeight = height / 2;
        dstRect.left = x - halfWidth;
        dstRect.top = y - halfHeight;
        dstRect.right = x + halfWidth;
        dstRect.bottom = y + halfHeight;

        if(moveState == MoveState.run){
            idleFab.draw(canvas, dstRect, null);
        }else{
            canvas.save();
            if(moveState == MoveState.hit){
                hitFab.draw(canvas,dstRect,null);
            }
            canvas.restore();
        }
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width /2;
        int hh = height/2;
        rect.left = x - hw;
        rect.right = x + hw;
        rect.top = y - hh;
        rect.bottom = y + hh;
    }

    @Override
    public void recycle() {
        Log.d(TAG,"recycle");
    }

    @Override
    public void processMessage(int event){
        Command cmd = commandQueue.peek();
        int isWin = -2;
        if(cmd == null) return;

        // 가위 바위 보
        if(event == cmd.getCommandState())
        {
            Log.d(TAG,"draw");
            // ui draw(비김)
            isWin = -1;
            //
        }else{
            if(event == Player.CommandState.rock.ordinal()){
                if(cmd.getCommandState() == Player.CommandState.scissors.ordinal()){
                    isWin = 1;
                }else{
                    isWin = 0;
                }
            }
            else if(event == Player.CommandState.scissors.ordinal()){
                if(cmd.getCommandState() == Player.CommandState.paper.ordinal()){
                    isWin = 1;
                }else{
                    isWin = 0;
                }
            }
            else if(event == Player.CommandState.paper.ordinal()){
                if(cmd.getCommandState() == Player.CommandState.rock.ordinal()){
                    isWin = 1;
                }else{
                    isWin = 0;
                }
            }
        }

        if(isWin == 1){
            // ui win
            Feedback good = new Feedback(UiBridge.metrics.center.x, GameplayScene.getLayoutGameView().bottom/2,400,250,R.mipmap.feedback_perfect);
            good.add(GameplayScene.Layer.ui.ordinal());
            commandQueue.poll().remove();
            GameplayScene.getTop().addScore(5);
            SoundEffects.get().playSE(R.raw.perfect);
        }else if(isWin == 0){
            // ui lose
            Feedback bad = new Feedback(UiBridge.metrics.center.x, GameplayScene.getLayoutGameView().bottom/2,400,250,R.mipmap.feedback_bad);
            bad.add(GameplayScene.Layer.ui.ordinal());
            GameplayScene.getTop().addScore(-5);
            SoundEffects.get().playSE(R.raw.bad);
        }else if(isWin == -1)
        {
            Feedback draw = new Feedback(UiBridge.metrics.center.x, GameplayScene.getLayoutGameView().bottom/2,400,250,R.mipmap.feedback_good);
            draw.add(GameplayScene.Layer.ui.ordinal());
            SoundEffects.get().playSE(R.raw.good);
        }
    }

}