package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.game.scene;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;

import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.R;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.Constant;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.GameScene;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.main.UiBridge;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.BitmapObject;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ui.Button;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.obj.ui.TextButton;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.ui.activity.RankActivity;

public class GameOverScene extends GameScene {
    private static final String TAG = GameOverScene.class.getSimpleName();

    public enum Layer {
        bg, obj, ui, COUNT
    }

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        initObjects();

        int mdpi_100 = UiBridge.y(100);
        //ValueAnimator animator =
        ValueAnimator anim = ValueAnimator.ofFloat(UiBridge.metrics.size.y, mdpi_100);
        anim.setDuration(500);
        anim.setInterpolator(new OvershootInterpolator());
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float value = (Float) animation.getAnimatedValue();
                scrollTo(value);
            }
        });
        anim.start();
    }

    private void scrollTo(float y) {
        int mdpi_100 = UiBridge.y(100);

        ArrayList<GameObject> bitmapObjs = gameWorld.objectsAtLayer(Layer.obj.ordinal());
        int bitmapCount = bitmapObjs.size();
        for (int i = 0; i < bitmapCount; i++) {
            BitmapObject bitmap = (BitmapObject)bitmapObjs.get(i);
            float diff = y - bitmap.getY();
            bitmap.move(0, diff);
            y += mdpi_100;
        }

        ArrayList<GameObject> uiObjs = gameWorld.objectsAtLayer(Layer.ui.ordinal());
        int count = uiObjs.size();
        for (int i = 0; i < count; i++) {
            Button btn = (Button)uiObjs.get(i);
            float diff = y - btn.getY();
            btn.move(0, diff);
            y += mdpi_100;
        }
    }

    private void initObjects() {
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;

        int cx = UiBridge.metrics.center.x;
        int y = UiBridge.metrics.center.y;
        int mdpi_100 = UiBridge.x(100);

        int buttonWidth = screenWidth * 1/2;
        int buttonHeight = screenHeight / 8;

        gameWorld.add(Layer.bg.ordinal(), new BitmapObject(cx, y, screenWidth, screenHeight, R.mipmap.black_transparent));
//        y += UiBridge.y(100);
        // Game Over Image ui로 변경하기
        BitmapObject gameover;
        int textSize = mdpi_100 / 3;
        gameover = new BitmapObject(cx, y,buttonWidth*2, (int) (buttonHeight*4), R.mipmap.gameover);
        gameWorld.add(Layer.obj.ordinal(), gameover);

        TextButton button;
        y += UiBridge.y(100);
        button = new TextButton(cx, y, "ReStart", textSize);
        button.setSize(buttonWidth, buttonHeight);
        button.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                GameplayScene.get().restart();
                pop();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button);

        y += UiBridge.y(100);
        button = new TextButton(cx, y, "Ranking", textSize);
        button.setSize(buttonWidth, buttonHeight);
        button.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                Intent intent = new Intent(Constant.currentContext, RankActivity.class);
                Constant.currentContext.startActivity(intent);
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button);

        y += UiBridge.y(100);
        button = new TextButton(cx, y, "Exit", textSize);
        button.setSize(buttonWidth, buttonHeight);
        button.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                SoundEffects.get().playSE(R.raw.button_click);
                // 로비씬으로 가야함
                pop();
                pop();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button);
    }
}
