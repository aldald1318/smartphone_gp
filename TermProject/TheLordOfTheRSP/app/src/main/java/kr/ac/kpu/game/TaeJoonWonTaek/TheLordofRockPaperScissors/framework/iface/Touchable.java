package kr.ac.kpu.game.TaeJoonWonTaek.TheLordofRockPaperScissors.framework.iface;

import android.view.MotionEvent;

public interface Touchable {
    public boolean onTouchEvent(MotionEvent e);
}
